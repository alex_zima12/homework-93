import {
    FETCH_EVENTS_FAILURE,
    FETCH_EVENTS_SUCCESS,
    FETCH_EVENTS,
    DELETE_EVENT_SUCCESS,
    DELETE_EVENT_FAILURE,
    DELETE_EVENT,
    CREATE_EVENT,
    CREATE_EVENT_SUCCESS,
    CREATE_EVENT_FAILURE,
    SHARE_EVENTS,
    SHARE_EVENTS_SUCCESS,
    SHARE_EVENTS_FAILURE,
    DELETE_SHARED_USER,
    DELETE_SHARED_USER_FAILURE,
    DELETE_SHARED_USER_SUCCESS
} from "../actionTypes";

export const fetchEventsSuccess = (events) => {
    return {type: FETCH_EVENTS_SUCCESS,events};
};
export const fetchEventsFailure = error => {
    return {type: FETCH_EVENTS_FAILURE, error};
};

export const fetchEvents = () => {
    return {type: FETCH_EVENTS};
};

export const deleteEventSuccess = (id) => {
    return {type: DELETE_EVENT_SUCCESS,id};
};
export const deleteEventFailure = error => {
    return {type: DELETE_EVENT_FAILURE, error};
};

export const deleteEvent = (id) => {
    return {type: DELETE_EVENT,id};
};


export const createEvent = (data) => {
    return {type: CREATE_EVENT,data};
};

export const createEventSuccess = (data) => {
    return {type: CREATE_EVENT_SUCCESS,data};
};
export const createEventFailure = error => {
    return {type: CREATE_EVENT_FAILURE, error};
};

export const shareEvents = (mail) => {
    return {type: SHARE_EVENTS,mail};
};

export const shareEventsSuccess = (data) => {
    return {type: SHARE_EVENTS_SUCCESS,data};
};
export const shareEventsFailure = error => {
    return {type: SHARE_EVENTS_FAILURE, error};
};


export const deleteSharedUserSuccess = (id) => {
    return {type: DELETE_SHARED_USER_SUCCESS,id};
};
export const deleteSharedUserFailure = error => {
    return {type: DELETE_SHARED_USER_FAILURE, error};
};

export const deleteSharedUser = (id) => {
    return {type: DELETE_SHARED_USER,id};
};