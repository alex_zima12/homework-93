import {takeEvery} from "redux-saga/effects";
import {facebookLoginSaga, loginUserSaga, logoutUserSaga, registerUserSaga} from "./users";
import {fetchEventsSaga,deleteEventSaga,createEventSaga,shareEventsSaga,deleteSharedUserSaga} from "./events";
import {
  CREATE_EVENT,
  DELETE_EVENT, DELETE_SHARED_USER,
  FACEBOOK_LOGIN,
  FETCH_EVENTS,
  LOGIN_USER,
  LOGOUT_USER,
  REGISTER_USER,
  SHARE_EVENTS,
} from "../actionTypes";

export function* rootSaga() {
  yield takeEvery(REGISTER_USER, registerUserSaga);
  yield takeEvery(LOGIN_USER, loginUserSaga);
  yield takeEvery(LOGOUT_USER, logoutUserSaga);
  yield takeEvery(FACEBOOK_LOGIN, facebookLoginSaga);
  yield takeEvery(FETCH_EVENTS, fetchEventsSaga);
  yield takeEvery(DELETE_EVENT, deleteEventSaga);
  yield takeEvery(CREATE_EVENT, createEventSaga);
  yield takeEvery(SHARE_EVENTS, shareEventsSaga);
  yield takeEvery(DELETE_SHARED_USER, deleteSharedUserSaga);
}

