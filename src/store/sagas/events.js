import {put} from "redux-saga/effects";
import axiosApi from "../../axiosApi";

import {
    fetchEventsSuccess,
    fetchEventsFailure,
    deleteEventSuccess,
    deleteEventFailure,
    createEventSuccess,
    createEventFailure,
    shareEventsSuccess,
    shareEventsFailure,
    deleteSharedUserSuccess,
    deleteSharedUserFailure
} from "../actions/eventsActions";
import {push} from "connected-react-router";

export function* fetchEventsSaga() {

    try {
       const response = yield axiosApi.get("/events");
        console.log(response, "fetchEventsSaga");
        yield put(fetchEventsSuccess(response.data));
    } catch(e) {
        if (e.response && e.response.data) {
            yield put(fetchEventsFailure(e.response.data));
        } else {
            yield put(fetchEventsFailure({global: "No internet"}));
        }
    }
}

export function* deleteEventSaga(id) {
    try {
        yield axiosApi.delete("/events/"+ id.id);
        yield put(deleteEventSuccess(id.id));
    } catch(e) {
        if (e.response && e.response.data) {
            yield put(deleteEventFailure(e.response.data));
        } else {
            yield put(deleteEventFailure({global: "No internet"}));
        }
    }
}

export function* createEventSaga(data) {
    try {
        yield axiosApi.post("/events", data);
        yield put(createEventSuccess(data));
        yield put(push("/"));
    } catch(e) {
        if (e.response && e.response.data) {
            yield put(createEventFailure(e.response.data));
        } else {
            yield put(createEventFailure({global: "No internet"}));
        }
    }
}

export function* shareEventsSaga(mail) {
    console.log(mail,"mail");
    try {
        const response = yield axiosApi.post("/events/mail", mail.mail);
        alert(response.data.message);
        yield put(shareEventsSuccess(mail.mail));
    } catch(e) {
        if (e.response && e.response.data) {
            yield put(shareEventsFailure(e.response.data));
        } else {
            yield put(shareEventsFailure({global: "No internet"}));
        }
    }
}

export function* deleteSharedUserSaga(id) {
    try {
        yield axiosApi.delete("/events/user" + id.id);
        yield put(deleteSharedUserSuccess(id.id));
    } catch(e) {
        if (e.response && e.response.data) {
            yield put(deleteSharedUserFailure(e.response.data));
        } else {
            yield put(deleteSharedUserFailure({global: "No internet"}));
        }
    }
}
