import {
    FETCH_EVENTS_FAILURE,
    FETCH_EVENTS_SUCCESS,
    DELETE_EVENT_SUCCESS,
    DELETE_EVENT_FAILURE,
    CREATE_EVENT_SUCCESS,
    CREATE_EVENT_FAILURE,
    SHARE_EVENTS_FAILURE,
    SHARE_EVENTS_SUCCESS, DELETE_SHARED_USER_SUCCESS, DELETE_SHARED_USER_FAILURE
} from "../actionTypes";

const initialState = {
    error: null,
    events: []
};

const eventsReducer = (state = initialState, action) => {
    switch (action.type) {
        case FETCH_EVENTS_SUCCESS:
            return {...state, events: action.events};
        case FETCH_EVENTS_FAILURE:
            return {...state, error: action.error};
        case DELETE_EVENT_SUCCESS:
            const newEvents = [...state.events];
            const id = newEvents.findIndex(p => p.id === action.id);
            newEvents.splice(id, 1);
            return {...state, events: [...newEvents]};
        case DELETE_EVENT_FAILURE:
            return {...state, error: action.error};
        case CREATE_EVENT_SUCCESS:
            return {...state, events: state.events + action.data};
        case CREATE_EVENT_FAILURE:
            return {...state, error: action.error};
        case SHARE_EVENTS_SUCCESS:
            return {...state, events: state.events + action.data};
        case SHARE_EVENTS_FAILURE:
            return {...state, error: action.error};

        default:
            return state;
    }
};

export default eventsReducer;

