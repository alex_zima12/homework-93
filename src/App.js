import React from 'react';
import {useSelector} from "react-redux";
import AppToolbar from "./components/UI/AppToolbar/AppToolbar";
import Routes from "./Routes";

const App = () => {
  const user = useSelector(state => state.users.user);
  return (
    <>
      <AppToolbar user={user} />
      <main>
          <Routes user={user} />
      </main>
    </>
  )
};

export default App;
