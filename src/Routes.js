import React from "react";
import {Redirect, Route, Switch} from "react-router-dom";
import Events from "./components/Events/Events";
import NewEvent from "./components/NewEvent/NewEvent";
import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";

const ProtectedRoute = ({isAllowed, redirectTo, ...props}) => {
  return isAllowed ?
    <Route {...props} /> :
    <Redirect to={redirectTo} />;
};

const Routes = ({user}) => {
  return (
    <Switch>
        <Route
            path="/"
            exact
            component={Events}
        />
      {/*<Route path="/" exact component={}/>*/}
      <ProtectedRoute
        path="/events/new"
        exact
        component={NewEvent}
        isAllowed={user}
        redirectTo="/login"
      />
      <ProtectedRoute
        path="/register"
        exact
        component={Register}
        isAllowed={!user}
        redirectTo="/"
      />
      <ProtectedRoute
        path="/login"
        exact
        component={Login}
        isAllowed={!user}
        redirectTo="/"
      />
    </Switch>
  );
};

export default Routes;