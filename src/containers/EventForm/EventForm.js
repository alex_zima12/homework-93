import React, {useState} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {createEvent} from "../../store/actions/eventsActions";
import DatePicker from "react-datepicker";

const EventForm = () => {

    const [state, setState] = useState({
        title: "",
        duration: ""
    });

    const [startDate, setStartDate] = useState(new Date());

    const dispatch = useDispatch();
    const error = useSelector(state => state.users.error);

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setState(prevState => {
            return {...prevState, [name]: value};
        });
    };
    const formSubmitHandler = e => {
        e.preventDefault();
        const data = {
            title: state.title,
            duration:state.duration,
            datetime: startDate
        }
        dispatch(createEvent(data));
    }

    let errorMes;
    if (error) {
        errorMes = <div className="text-danger mb-3">{error.message}</div>
    }

    return (
        <form
            onSubmit={formSubmitHandler}
            className="container mt-3"
        >
            <div className="form-group">
                <label htmlFor="InputTitle">Title</label>
                <input type="text"
                       className="form-control"
                       id="InputTitle"
                       placeholder="Enter title"
                       name="title"
                       value={state.title}
                       onChange={inputChangeHandler}
                       required
                />
            </div>
            <DatePicker
                selected={startDate}
                onChange={date => setStartDate(date)}
                required
            />
            <div className="form-group">
                <label htmlFor="InputDuratione">
                    Enter how long the event will be valid</label>
                <input type="text"
                       className="form-control"
                       id="InputDuration"
                       placeholder="Enter title"
                       name="duration"
                       value={state.duration}
                       onChange={inputChangeHandler}
                       required
                />
            </div>
            {errorMes}
            <div>
            <button type="submit" className="btn btn-primary mt-3"> Create New Product</button>
            </div>
        </form>
    );
};

export default EventForm;