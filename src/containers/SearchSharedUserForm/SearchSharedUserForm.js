import React, {useState} from 'react';
import {useDispatch} from "react-redux";
import {shareEvents} from "../../store/actions/eventsActions";


const SearchSharedUserForm = () => {
    const dispatch = useDispatch();
    const [state, setState] = useState({
        mail: ""
    });

    const inputChangeHandler = e => {
        const name = e.target.name;
        const value = e.target.value;
        setState(prevState => {
            return {...prevState, [name]: value};
        });
    };
    const formSubmitHandler = e => {
        e.preventDefault();
        dispatch(shareEvents({...state}));
    }

    return (
        <form
            onSubmit={formSubmitHandler}
            className="container mt-3"
        >
            <div className="form-group">
                <label htmlFor="InputTitle">Friend mail</label>
                <input type="email"
                       className="form-control"
                       id="InputTitle"
                       placeholder="Enter your friend the mail you want to share your events"
                       name="mail"
                       value={state.mail}
                       onChange={inputChangeHandler}
                       required
                />
            </div>

            <div>
                <button type="submit" className="btn btn-primary m-3"> Search </button>
            </div>
        </form>
    );
};

export default SearchSharedUserForm;