import React from "react";
import {NavLink} from "react-router-dom";
import {useDispatch} from "react-redux";
import {logoutUser} from "../../store/actions/usersActions";
import defaultAvatar from "../../assets/images/image_not_available.png";
import {apiURL} from "../../constants";

const mystyle = {
    width: "50px",
    height:"50px"
};

const UserMenu = ({user}) => {

    const dispatch = useDispatch();

    const logout = () => {
        dispatch(logoutUser());
    };

    let cardImage = defaultAvatar;
    if (user.avatar) {
        cardImage = apiURL + "/uploads/" + user.avatar;
    }

    return user && (
        <>
            <div>
                <NavLink className="font-weight-bold text-light m-3" to={"/events/new"}>Add New Event</NavLink>
            </div>
            <div>
                <div className="font-weight-bold text-light ">Hello, {user.displayName || user.username}!
                    <img src={cardImage}
                         className="rounded circle rounded-circle m-3"
                         style={mystyle}
                         alt="user"
                    />
                </div>
                <button className="font-weight-bold" onClick={logout}>Logout</button>
            </div>
        </>
    );
};

export default UserMenu;