import React from "react";
import {NavLink} from 'react-router-dom';
import AnonymousMenu from "../../Menu/AnonymousMenu";
import UserMenu from "../../Menu/UserMenu";

const AppToolbar = ({user}) => {

  return (
      <>
        <nav className="navbar navbar-dark bg-primary row">
          <NavLink className="font-weight-bold text-light text-decoration-none display-4 ml-3" to={"/"}>
            Events Calendar</NavLink>
        </nav>
        <nav className="navbar navbar-dark bg-primary row">
          <div className="container">
            {user ?
                <UserMenu user={user} />
                :
                <AnonymousMenu />
            }
          </div>
        </nav>
      </>
  );
};

export default AppToolbar;