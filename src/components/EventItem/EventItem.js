import React from 'react';
import {useDispatch, useSelector} from "react-redux";
import {deleteEvent} from "../../store/actions/eventsActions";
import Moment from 'react-moment';

const EventItem = ({event} ) => {
    const dispatch = useDispatch();
    const error = useSelector(state => state.events.error);
    const user = useSelector(state => state.users.user);

       const  deleteItem = () => {
        dispatch(deleteEvent(event._id));
    };

    let errorMes;
    if (error){
        errorMes = <div className="text-danger mb-3">{error.error}</div>
    }

    return event && (
        <li className="list-group-item">
            <h1><Moment format="DD/MM/YYYY">
                {event.datetime}
            </Moment></h1>
            <b className="d-block">{event.title}</b>
            <p>Author: {event.user.username}</p>
            <p>{event.duration}</p>
            {user.username === event.user.username ?
                <button
                    className="text-danger mt-3"
                    onClick={deleteItem}>
                    Delete
                </button>
            : null}
            {errorMes}
        </li>
    );
};

export default EventItem;