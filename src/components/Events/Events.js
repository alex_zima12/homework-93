import React, {useEffect} from 'react';
import {useDispatch, useSelector} from "react-redux";
import {NavLink} from "react-router-dom";
import {fetchEvents,deleteSharedUser} from "../../store/actions/eventsActions";
import EventItem from "../EventItem/EventItem";
import SearchSharedUserForm from "../../containers/SearchSharedUserForm/SearchSharedUserForm";

const Events = () => {
    const dispatch = useDispatch();
    const user = useSelector(state => state.users.user);
    const events = useSelector(state => state.events.events);
    const error = useSelector(state => state.events.error);

    useEffect(() => {
            dispatch(fetchEvents());
    }, [dispatch]);

    let errorMes;
    if (error){
        errorMes = <div className="text-danger mb-3">{error.error}</div>
    }

    let allEvents;
    if (events) {
        allEvents = events.map(event => {
            return <EventItem
                key={event._id}
                event={event}
            />
        })
    }

    const deleteUser = () => {
        dispatch(deleteSharedUser(user._id));
    }

    console.log(user);
    let sharedUsers = [];
    if (events) {
       events.map(event => {
            event.users.map(user => {
                sharedUsers.push(<li key={user._id}><button className="btn btn-primary m-3" onClick={deleteUser}>×</button>{user.username} </li>);
            })
        })
    }

    return  (
        <div>
            {!user ?
                <div className="container mt-3">
                <p className="font-weight-bold">
                    Only logged in user can manage events in the application
                </p>
                <NavLink className="font-weight-bold col-2" to={"/login"}>Sign In</NavLink>
                </div>
                : <div className="container">
                    <SearchSharedUserForm/>
                    <div>
                        <p>
                          Here are users exchanging events
                        </p>
                        <ul>
                        {sharedUsers}
                        </ul>
                    </div>
                    <h1>
                        Here are your main events
                    </h1>
                    <ul className="list-group">
                    {allEvents}
                    </ul>
                </div>
            }
            {errorMes}
        </div>
    );
};

export default Events;