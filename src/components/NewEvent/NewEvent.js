import React from "react";

import EventForm from "../../containers/EventForm/EventForm";

const NewEvent = () => {

    return (
        <div className="container">
            <h1>New Event</h1>
            <EventForm/>
        </div>
    );
};

export default NewEvent;